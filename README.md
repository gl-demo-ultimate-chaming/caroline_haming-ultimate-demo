# Ultimate Demo - CAROLINE HEAMING

Welcome to my demo project! 

## How to use this template

To use this project, you can import the project to your personal Ultimate group. From there, you will follow the steps in the Issues that correspond with each week's async work assignments.

## Table of Contents
- [Start Here: Welcome to the Apprenticeship!](#1)
- [Week 1: Create](#2)
- [Week 2: Verify](#3)
- [Week 3: Plan](#4)
- [Week 4: Secure](#5)

## GitLab - Getting started

### Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/smorris-secure-app-demo1/ultimate-demo-template.git
git branch -M main
git push -uf origin main
```
